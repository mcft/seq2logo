#!/usr/bin/env python
""" This script can read a psfm file and compute the IUPAC consensus from it

The IUPAC consensus is printed using the fasta format
$ cat > psfm.txt
#Position Specific Frequency Matrix
A C G T
1 G 0.01 0.01 0.97 0.01
2 C 0.01 0.97 0.01 0.01
3 A 0.90 0.08 0.01 0.01
4 G 0.01 0.01 0.49 0.49
5 G 0.01 0.01 0.97 0.01
6 G 0.01 0.01 0.90 0.08
7 T 0.01 0.21 0.29 0.49
8 A 0.97 0.01 0.01 0.01

$ psfm2consensus_iupac.py -t 0.05 psfm.txt
>consensus_iupac
GCMKGKBA
"""
import sys, argparse
import numpy as np

# Get command line args
parser = argparse.ArgumentParser()
parser.add_argument("-t", "--threshold", default=0.05,
                    help=("threshold for how big a fraction must be to be "
                          "cosiddered significant"),
                    type=float)
parser.add_argument("psfm", help="position specific frequency matrix")
args = parser.parse_args()

IUPAC = {
 'A'    : 'A',
 'C'    : 'C',
 'G'    : 'G',
 'T'    : 'T',
 'AT'   : 'W',
 'CG'   : 'S',
 'AC'   : 'M',
 'GT'   : 'K',
 'AG'   : 'R',
 'CT'   : 'Y',
 'CGT'  : 'B',
 'AGT'  : 'D',
 'ACT'  : 'H',
 'ACG'  : 'V',
 'ACGT' : 'N'
}

# Extract Sequence data
alphabet = None
seq_dat = {}
with open(args.psfm) as f:
   for l in f:
      l = l.strip()
      if l == '': continue
      if l[0] == '#': continue
      tmp = l.split()
      if alphabet is None:
         alphabet = tmp
      else:
         pos = int(tmp[0])
         seq_dat[pos] = [float(x) for x in tmp[2:]]

# Create IUPAC representation
consensus_iupac = []
for pos in sorted(seq_dat):
   base_array = []
   for i, freq in enumerate(seq_dat[pos]):
      if freq >= args.threshold:
         base_array.append(alphabet[i])
   consensus_iupac.append(IUPAC[''.join(sorted(base_array))])

sys.stdout.write(">consensus_iupac\n%s\n"%(''.join(consensus_iupac)))
